readme.txt
=================================

This file contains information on the dnd.htm file, the
3rd (v3.5) Edition D&D character generator. 
Actually, it's a bunch of random notes regarding the project
with a change log toward the bottom.

Written by Jesse Smith <slicer69@hotmail.com>
http://dnd3rd.sourceforge.net
Project: dnd3rd on freshmeat.net (search "dungeons and dragons")
Date: Sept 6, 2003.

The generator allows the user (that's you) to input some basic
information. This includes the character's race, class, level,
religion and name. You can also roll abilities, select skills, feats
and pick out your equipment. As of version 0.9, you can also
select which spells your character will use.

The generator lets you display/print a blank character sheet or
a sheet with all of the field's filled in. The print/display may
look different on different browsers, so I didn't do anything
fancy here. I would like to suggest using a standard blank sheet and
use the CG to fill in the values.

This generator is released under the GPL (GNU Public License) version 2.
This means, in short, that you can use and copy the code I've worked on
freely. However, if you change it, you have to give everyone else
access to the changes and you can't include this code in a closed source 
product/project.

I won't turn down donations. So, if you feel like throwing a few
dollars my way, please send me an e-mail.

Please see the change log below to see what's new in this version.



Version 4.5
==============================

1. Fixed bug where characters could not select Mounted Archery 
   as a feat.




Version 4.4
==============================

1. Fixed problem with points assigned to
   skills for Elves and Half Elves. Racial
   bonuses were not adding properly.





Version 4.3
==============================

1. Wizards and Sorcerers can now select the
   spell Detect Magic from the list of level zero
   spells.

2. Fixed some more spelling errors on the character sheet.

3. Characters with the Ride skill now see their
   Dexterity bonus on the character sheet.





Version 4.2:
==============================

1. Fixed bug where wizards and fighters were not receiving
   their bonus feats when they level up.

2. Fixed loading bug where fighters and wizards would
   not receive bonus feats.

3. Fixed problem with selecting Rapid Shot. Some players were
   not getting the feat.





Version 4.1
==============================

This release fixes a few problems with Rangers and the
feats they can select.

1. Rangers can now get two weapon defence when the two weapon
   fighting style is selected at second level.

2. Rangers can now get all the feats they are entitled
   to due to their combat styles. Rangers are treated
   as having either Rapid Shot or Two Weapon Fighting.





Version 4.0
==============================

1. Fixed bug where elves would not receive the
   Move Silently skill.





Version 3.9
==============================

Characters can now equip masterwork weapons and shields.




Version 3.8
==============================

1. Paladins now get Divine Grace. At level 2 and higher
   paladins get a saving throw bonus equal to their
   charisma modifier.

2. Alignment conflicts no longer prevent character
   generation. Instead conflicts are dislpayed as warnings.




Version 3.7
==============================

1. If the character sells an item they are
   using (equiped) then the armour bonus
   is removed and the character must re-equip.

2. Fixed some minor save/load issues dealing with
   equiped items.




Version 3.6
===============================

1. Fixed typo in Druid special class skills.
   Thanks to Eric for pointing out this error.

2. Mentioned that the generator is for D&D 3.5
   in the title. Thanks to Mike for pointing
   out the confusion. Also mentioned this in
   the FAQ section.

3. Various game notes can be kept in the Extra Notes
   section at the bottom of the main page.

4. Players can now equip their character with armour
   and a shield. This is via the Equip Character
   button on the main page. The bonus to armour
   is displayed on the final character sheet.





Version 3.5
===============================

This release of the character generator fixes a few
long-standing bugs and tries to make the character
sheet look more organized.

1. Fixed bug where Halflings get extra points
   on their saving throw each time they
   view the character sheet.

2. Boxes next to melee, ranged, saving throws and inititive are
   now added and the sum is displayed.

3. Skills are no longer displayed with trained or untrained
   indicators on the final sheet, since all all skills shown
   can be used.
   Trained or untrained is still shown on the Skills page.

4. Druids now get the Druidic language automically.

5. It is now impossible for a character to unlearn a
   natural language. For example, elves must take
   elven, half orcs must take orc, druids must have druidic.

6. Skills are now displayed on the final sheet showing rank,
   modifier and total.

7. Classes now get access to all of their languages. This
   fixes a bug with Druids,



Version 3.4
===============================

I've tried to add some friendliness and some more
flexiblity to uses who are upgrading their characters.

1. When selecting a new Starting Class, all other classes
are removed. This is designed to make the class selection
to new players and low level characters.

2. When selecting feats, you can hover the mouse over the
   feat name to get a brief description of what the feat does.

3. Player can now add equipment items which are not in the
   book. These are probably items assigned by the DM or
   found while in the game. These items may be added at
   the bottom of the Equipment window.

4. Equipment items hand added by the player can now be saved
   and loaded later with the character.



Version 3.3
===============================

When loading a character from a text file, the
Generator now correctly displays the character's
race.



Version 3.2
===============================

This release does not change much code. I am
clearifying the license and checking some output.




Version 3.1
===============================

More pretty, more user-friendly! That's the
name of this release. Well, actually, the
name is 3.1, but our focus is making the
interface more friendly.

1. When a class conflicts with alignment, the
reason is given, rather than just saying there
are restrictions on that class.

2. Cleaned up the Select Feats page. Aligned text.
3. Minor adjustments to the main page and Language page.
4. Added descriptive text to god/goddesses on main page.




Version 3.0
===============================

1. Updated the FAQs.
2. Changed the Feats page to display "Fighter Bonus Feat"
   and "Wizard Bonus Feat" next to the respective feats,
   instead of those cryptic symbols.



Version 2.9
===============================

This version address a few issues which have
been stumbled upon, either by myself or
those under-paid and over-worked bug reporters.
Also, the Equipment/Wasterwork selection was finally
setup to work properly. You may now purchase regular
and masterwork equipment seperately. Please see the
Help section dealing with Equipment for details.

1. When using the Level Up feature, it is
   now impossible to raise an individual
   class level above twenty.
   This avoids a few problems with
   calculating character stats and abilities.

2. It has been brought to my attention
   that when a character gains bonus ability
   points in Intelligence and Constitution
   their skill points and hit points do
   not raise retro-actively.
   Because of the many complicated problems
   this would cause, this problem won't
   be fixed directly (please see the FAQs
   page for more details) but you can use the
   Level Up function to get around this.

3. Once the Level Up feature has been used
   you may not longer unassign bonus ability
   scores. This is to prevent gaining skill
   or hit points and then putting the ability
   points in another place.

4. Wasterwork equipment now makes a lot more sense.
   You may buy any combination of regular or masterwork
   equipment now from the Equipment window. There
   is no longer a requirement to have all items of one type.

5. Saveing/Loading characters was updated to handle new
   equipment/masterwork selection.





Version 2.8
===============================

This version is directed as making selecting
Feats and Skills a little easier to understand.

1. Some help pages were adjusted for look.

2. When the user attempts to select a
   feat for which they do not have the prerequites,
   an alert box is displayed with a list of the
   requirements for the feat. At this time,
   the list is not recursive. That is, if someone
   selects a feat which requires Power Attack,
   you will see "Power Attack", but not the
   requirements for Power Attack.




Version 2.7
===============================

This version attempts to add some ease of use
for new players.  Some code clean up and minor
bug fixing was also done.

The changes to the CG in this version are:
1. When leveling up a character,
   the number next to the character class
   on the main page reflects the number of
   levels the character has in that class.

2. When checking to see if a character qualifies
   for a feat, half-orcs now take their
   -2 to intelligence...assuming it does not
   force their intelligence score below 3.

3. Minor code clean up while removing feats.

4. If the user attempts to select a feat without
   the requirements, a message is displayed
   telling them of this.

5. When opening pop-up windows in Internet Explorer
   the new windows are now resizable.

6. Added an entry to the FAQs about dealing
   with Internet Explorer's script blocking
   feature.

7. Added headers to the equipment page, showing
   seperation between Weapons, Armor, Tools, 
   Clothes and Mounts
   
8. Updated the About window to better reflect
   supported and non-supported web browsers.

9. Made sure Half-Orc intelligence scores are
   always 3 and above. On the main page these
   show as 5 or above for Half-Orcs.

10. Made small corrections to spelling and
    grammer in the Help pop-up windows.

11. Fixed skill display for Halflings. Many
    skills were not displayed for Halflings
    before.




Version 2.6
===============================

This version, which is long over-due,
tries to address some problems we've had
running the CG in Internet Explorer. Versions
5.5 and 6.0 of IE have had problems, so I thought,
displaying the pop-up windows.
As it turns out, during my debug process, I
discovered most of the problems were just that
IE processes Java Script very slowly. This caused
some people (myself included) to give up before
the window content was displayed. There was a legit
problem with the final character sheet with stats being
displayed. That has now been fixed.

Changes to this version:
1. When a character levels up, the check box
   on the main page, next to the new character
   class, becomes checked.

2. All character information is now displayed
   in Internet Explorer 5.5. I believe the
   CG should now work in all versions of IE,
   as well as Opera, Firefox and Konqueror.




Version 2.5 (Update)
================================

While not a release of code or a change annoucement, I
am happy to be able to say that Firefox 1.0 users
have e-mailed me to let me know the CG works in Firefox 1.x.
Thank you.

1. When a character levels up, the check box on
   the main page next to the new class becomes
   checked.




Version 2.5
================================

Okay, this release is one that I've been focused on for
a while now. Now that we can save/load characters, I
now bring to you the ability to "level up" a character.
At the bottom of the main page, you will find a "Level Up"
button. This will allow you to gain a level in a given class.
Please see the "Level Up Help" button for more information.


1. Make the Bonus Ability Points box set to "0"
   when a character is loaded.

2. Fixed a bug that prevents a 19th or 20th level wizard
   from getting all of his/her ninth level spells.

3. Put the spells that sorcerers and bards can learn into seperate
   functions for modularity.

4. Fixed "Close" button to the spell selection page.

5. Added the ability to level up a character via the
   "Level Up" button on the main page.
   This automatically gains one character level and
   adds the skill points, feat points, spells, ability score points,
   hit points and saving throws.





Version 2.4
=======================================

I am now going to address something that people have been
asking for since version 1.0: the ability to save and re-load
characters. This is something I've put off for a while. The main
reason being that JavaScript (the language used to create the CG)
has no file support. That is, JavaScript does not have the commands
to save files to your drive for later use. There are ways
around this, using MS-ActiveX. However, that is not an attractive
option.

My solution was this: Dump all of the character data into a single
text box and you, the user, may copy/paste that data into a text
file. When you wish to reload a character for later tweaking/printing,
you can copy/paste that data back into the generator.

It works like this: You create your character as normal. Then,
click the Save Character button at the bottom of the main window.
A new window pops up, with a text box. That text will be your
character info. Copy/Paste that data into a text file in Notepad
or VI or some other text editor. When you wish to use your character
again, click on the Load Character button. Copy/Paste the data
from the text file into the text box and click the Load button. Your
character will be restored and ready for use.

This new feature(s) is a push in a rather new direction for the
CG's development. If you find any bugs or have any suggestions, please
e-mail me.


The changes present in this release are:

1. Editted the spell code to avoid script errors when selecting
   bard spells.

2. Created the Save Character window. This allows a user to copy/paste
   a character's data into a text file for later use.

3. Fixed a few typos in the FAQs.

4. Added ability to re-load a character from a saved
   block of data/text. Data from a saved text file
   can be copy/pasted into a Load Window and the
   old character will be restored.

5. Some minor code tweaks in the Help functions.




Version 2.3
=======================================

This release patches up some more Feat issues.
Wizards are now restricted to using their
bonus feats for Wizard Feats only. Also, the
Feat selection page has been tweaked a little.
Monks and Rangers have also ben greatly improved upon.

1. Wizard Bonus Feats are now used to select
   Wizard feats only. Please see the Help with Feats
   button for more information.

2. Feat selection was fixed. Before wizards and fighters
   had to select their bonus feats before selecting general
   feats. This has been corrected.

3. Error messages for selecting feats has been updated.

4. The Feat selection page is now looks cleaner. It displays
   bonus Fighter and Wizard feats only when the character has
   those classes.

5. Rangers now get Endurance as a bonus feat at
   3rd level. Before this was treated as a class special
   ability, rather than a feat. Endurance now is shown
   as selected by 3rd level rangers on the Feats page.

6. Added the Bonus Feats page. This is accessed using the
   Select Bonus Feats button, just under the regular Select Feats
   button.

7. Monk now get their bonus feats. At levels 1, 2 and 6
   a monk gets one bonus feat which may be used to buy
   monk specific feats. Please see Help with Bonus Feats for
   more information.

8. Rangers may now select their combat style through the
   Select Bonus Feats button. This grants a ranger their bonus
   feats at levels 2, 6 and 11. These feats are not shown as
   selected on the regular Select Feats page.

9. Spell selection is now cleaner. Only classes which the
   character have are displayed on the Spell Selction page.




Version 2.2
=======================================

This release addresses a minor issue with Feats.
Specifically, the generator used to use bonus fighter feats
as any other feat. However, fighter bonus feats may be
used to select certain combat feats only. This release
corrects which fighters may select with their bonus
feats.

1. Fighters may now use their bonus feats to
   select bonus fighter feats only. Please see
   the Help with Feats button for more information.




Version 2.1
=======================================

In a new push to add some features to the Character
Generator, I have added the ability to select masterwork
weapons and armor. This is done on the Equipment Screen.
A new check box has been added next to each weapon or
armor piece. The selection process is a little odd, 
but bare with me.
All items of one type must be either masterwork or not. So
if you have a two long swords, both must be either
masterwork or neither can be masterwork. You cannot
have one of each.
Checking or unchecking the Masterwork box will attempt
to make the item in your inventory masterwork. You
will be informed if you have enough money to do this.
Also, you are credited properly, if you sell masterwork
items.

Please see the Help With Equipment button in the CG for
more information.

Changes in this release...

1. Added masterwork armor and weapons to Equipment Screen.

2. Masterwork bolts and arrows now use the pricing rules for
   D&D v3.5.





Version 2.0
=======================================

Alright. I've decided to add some more important
functionality to the character generator. Mostly,
this is to make use a bit more simple and to
handle odd tasks.
For example....

1. If you use all of your feat points, you
   now receive a message declaring that when you
   try to select a new feat.

2. If you try to buy a skill that you cannot afford,
   you will be told you do not have enough skill
   points.




Version 1.9
=======================================

I have made some minor racial corrections for this release.
This is important for those of you playing Gnome
or Halfling characters.

1. Halflings now get their +1 to Base Attack Bonus
   and their +1 to Armor Class.
   Previously, they just got bonuses for their dexterity,
   this also factors in their size.

2. Gnomes now get their added +1 to Base Attack Bonus
   and their +1 to Armor Class. This is due
   to their small size.




Version 1.8
=======================================

It seems that I haven't been giving monks all the credit
they deserve. This release just touches up the monk class.

These changes are, in order:
1. Gave monks their wisdom bonus to armor class.
   Monks get an armor class bonus equal to their wisdom
   modifier.

2. Monks should get an armor class bonus based on their
   class. This acts as a special ability, giving a monk
   an armor class bonus of their monk level divided by five.




Version 1.7
========================================

This version fixes the money issue. It also makes some
code clean up. Spell displaying has also been fixed a bit.

The changes are, in order:
1. Changed money calculating formula for players over 2ed level.
   It was very wrong. It has now been fixed, matching the table 5-1
   on page 135 of the Dungeon Master's Guide.
2. Wizards cannot not get level 0 spells in their prohibated
   schools of magic.
3. Wizard spells are now displayed with the spell school.
4. A wizard with intelligence less than ten gets no spells.





Version 1.6
========================================
I have taken some time to patch up a couple of bugs
in this release. I am also hoping to make the
generator easier to use and keep up to date.

The changes to this release are as follows:
1. Removed a bug in the FAQ function. This
   was causing JavaScript errors.
2. Added Check for Updates button. For those using
   dial-up, getting/checking for updates is a pain, due to
   the size of the generator. The Update button
   will open a very small HTML page on the server,
   and display the current offical version of
   the Character Generator.
3. The way skills are displayed for Humans was fixed.
   Skills with 0 ranks are no longer displayed.




Version 1.5
========================================
This version cleans up some minor bugs and adds
some new usablity. Many thanks to everyone
who submitted bug reports and feature requests.

The changes in this version of the character
generator are:
1. When you select "Other" as your religion it now
   displays the religion name you give on your
   character sheet.
2. Fixed the way hit points are calculated. Points
   were typically too low for higher level characters.
3. The character sheet now displays whether a skill
   is trained (T) or untrained (UT).
4. Fixed error where only Elves and Half-Elves could get
   Listen, Search and Spot. These skills when selected
   by other races (like Dwarves) now show up on
   the final character sheet.





Version 1.4
========================================
Due to some ironic timing, I starting getting bug
reports for version 1.2 right after releasing 1.3.
I would like to thank JS for pointing out a bug in
the way in which the CG calculated skill points
for Half-Orcs.

The changes to this release are as follows:
1. Fixed bug that gives Half-Orcs too many skill points.
   Half-Orcs get a -2 to intelligence, lowering their skill points.
2. Half-Orcs were getting an extra bonus language. This has
   now been corrected.
3. You can now set the amount of gold your character has.
   In the Equipment window, enter the amount of gold you want
   and click the "Set Money to This Value" button.
4. Fixed caluclation of gold pieces when gold is over
   100 pieces.
5. Made major changes to final character sheet output.
   Instead of text boxes, the skills, feats, equipment,
   languages and spells are now simply printed to the
   bottom of the sheet. This should make the sheet
   much more printer-friendly.




Version 1.3
========================================
I have been receiving a growing number of questions
about the character generator. About where is can be used,
about new features and how to use it. To make things
easier for myself and the CG's users, I have created
a frequently asked questions page. This page
can be found at http://slicer69.tripod.com/dnd/faqs.htm
You can also access the FAQs via the character generator
by clicking on the FAQs button at the top of the page.

In this release I have also touched up some of the Help
sections. As there were no bugs reported after the last
release, there are no other fixes to report. If you find any
bugs or have any feature suggestions, please let me know.




Version 1.2
========================================
I have come across another bug in testing...Okay, so I was making
more characters. Here are the changes for this release:

1. The skill "knowledge geography" now displays "int" (for
intelligence) as the modifier. Previously no modifier was given.
2. Fixed definition. The hide skill should always be defined as
"HIDE_SKILL" not "HIDE" to avoid conflict with JavaScript.
3. Fixed skills selection so that you can remove skill ranks.
Must have broken this previously.
4. Fixed Bard spell selection. Bards from level 2 to level 4
were getting spells of a 4th level Bard.
5. According to page 12 of the Player's Hand Book, humans should
be able to learn elven. This has been fixed.
 



Version 1.1
========================================
This is a bug fix release. I have fixed a problem
that races (mostly Elves, Half Orcs and Halflings) have
when selecting feats. I have also fixed a bug keeping
Barbarians from getting Martial Weapon Proficiency.

1. Made race adjustment for feat pre-requisets. This allows
   Elves with lower dex, for example, to get dex-based feats.
2. Fixed a few spelling mistakes that kep Barbarians from
   getting Martial Weapon Proficiency.






Version 1.0
=========================================
Okay, let's key some music from 2001...Dun, dun..dudun!

This is a purely bug-fix release. Problems that I've found or
that you folks have been good enough to send in are
fixed here.

Without further ramble, here are the changes made in version 1.0:

1. Fixed spelling mistake in Race Help.
2. Added text to Spell Help.
3. Races that gain or lose ability points, now have adjusted inititive,
   armor class, fortitude, relfex saves and attacks.
4. Halfling strength modifiers are now calculated properly.
5. The number of spells a magic user can learn (per level) is now
   displayed next to the spells of that level.
6. Added a Reset button in the Wizard Spell Selection. Clicking
   this button re-claims all wizard spells an resets any specialization.
7. Classes now get their class feats automatically selected. For example,
   Fighters now start with weapon and armor proficiencis. These
   are displayed on the Feat Selection page and on the final
   character sheet.
8. You now cannot remove automatic class feats. For example, Fighters
   get Tower Shield Proficiency. This cannot be removed.




Version 0.9
===================================
Okay, I finally did it. The character generator now supports spell selection.
This is a major release and everyone is encouraged to upgrade to v0.9.
Since this is a big feature boost, there are likely bugs that I haven't found
yet. Please let me know if you find anything out of place. If possible, please
tell me where you found the bug, what your character looked like at the time
and what you think the problem is. For example:

  "Wizards should get two spells per new level. However, my second lvl wizard
   only gets one new spell for her second level. See page 57 of the PHB."

I have also fixed a lot of bugs and added some new features. Most of these
deal with how the generator handles feats and related skill ranks.

There is a known bug in Wizard spell selection. If you choose to
replace a high level spell with a lower level one, then remove the
lower level spell, you may not go back to the higher level spell.
However, if you clear the spell list by changing your specialized schooll,
then you get the high level spell back.


Thank you very much for your support. Here are the changes made in v0.9
1. If you select skills, equipment or feats and then swap your ability scores,
   the skills, equipment, feats, etc are wiped. This keeps players from
   selecting lots of skills and them swaping their intelligence.
2. Selecting Toughness as a feat now gives you character the
   extra 3 hit points.
3. Now selecting/unselecting a feat will give you the skill bonus 
   for that or saving throw bonus for that feat.
4. Fixed some spelling errors in the code for feats.
5. When you have feats, which give you skill bonuses, you cannot
   remove those skill bonuses, without removing the feat first.
6. Fixed display for silver coins. Amount should be correct now.
7. When you close the equipment window and come back later, the
   window now remembers the items you previously had.
8. Updated this file.
9. Wizards can now select their spells, their specialized and
   prohibited schools. Spells are displayed on the final character
   sheet.
10. Sorcerers can now select their spells. Sorcerer spells are
    displayed on the character sheet in the arcane spell section.
    Note: Sorcerers cannot select arcane spells Rary's Mnemonic Enchancer 
          or Mordenkainen's Lucubration.
11. Bards can now select their spells. Bard spells are displayed
    in the "Bard Spell" window of the character sheet.




Version 0.8
===================================
With this version, I hope to clean up some display issues. Nothing major
but I suggest you upgrade anyway. It can't hurt :)
I would like to thank Áncor González Sosa for help in finding some coding
bugs in this release. There was an error in the random
dice rolls. This has now been corrected.

The changes made in the 0.8 release are as follows:
1. Made text boxes in the character display window larger so
   that text doesn't wrap around.
2. Rounded off silver coins, having 0.3334 silver looks funny.
   Silver should now display as a whole number.
3. Áncor González Sosa found a bug that makes dice rolls uneven. He points
   out that with the old system, dice rolls were three times more
   likely to come up "1", than, say "6" in ability score rolls.
   This has been corrected, so there should now be a even chance
   of rolling a "1" through "6".
4. Áncor González Sosa also pointed out that some function
   make random dice rolls, without calling Roll_Dice(). This
   has been corrected to modularize the code.



Version 0.7
====================================
This version didn't change much, but I've added one cool feature.
You can now randomly generate character names by pressing the "Random Name"
button next to the Character Name field. The code for this was provided
by David A. Wheeler. You can reach him and see his original code at
http://www.dwheeler.com/totro.html
Many thanks, David.



Version 0.6
====================================
This version cleans up some code and includes
a few suggestions from my brave and tiredless testers.
Thanks to Corey for his suggestions.

The changes are, as follows:
1. When you manually enter your ability scores,
   any previously rolled scores are carried over into
   the ability window.



Version 0.5
====================================
Okay, here we have some minor bug fixes and adjustments
into the 3.5 version. Many thanks to BB and IZ for pointing
out some mistakes. There have been a lot of add-ons and
changes to the code in this release. I tried very hard not
to break anything, but please let me know if something
stopped working.

The changes are, in order:
1. Made class/alignment restrictions. These
   apply, only for non-multi-class characters.
2. Fixed spelling mistake for Rogue.
3. Starting money is now taken from a table in the DMG.
   For first level characters, the formula in the PHB is
   used (Equipment section), but second level and higher
   get (3 ^ level) * 100 gold.
4. Corrected spelling mistakes in gods' names.
5. If the character class, race or alignment change, then ability
   scores are reset.
6. You cannot display a filled-in character sheet now without
   first creating a character. (Roll ability scores.)
7. Skills, feats, languages and equipment is now reset whenever you
   re-roll your ability scores. 
8. You can now select feats, skills, languages and equipment, close their
   windows and reopen them without losing your selections.
9. Spell checked this file again.
10. Due to popular demand, you can now manually enter ability scores.
    The help window has been updated to support this.
11. Fixed alignment restriction with Druids.
12. Fixed money display. Added PP field for money.
    You money is now broken up into each field so we
    won't have crazy amounts of gold for high level
    characters.
13. Added modifiers to will, reflex and fortitude saves. This should make it
    easier to figure out your saving throws and update them later.
14. Also added modifiers to melee and ranged attacks. There is now
    a break down of inititive and armor class too.
15. Placed ability modifier next to skills, on the character
    sheet.
16. Adjusted hit points calculation so that Elves, Dwarves 
    and Gnomes get the correct number of hip points.




Version 0.4
====================================
Ok, now I'm getting somewhere. The last CG release (0.3)
didn't come back with any bug reports. This tells me that either
the people using the generator have given up on me, or I've fixed
most of the problems. After holding my breath for a few days, I
started adding some new features (are you paying attention to the
order of things here, Microsoft?). At this point, I am trying to
put more into the generator, to give a more complete character.
Please let me know what you think. I'm a slave for feedback.

Here are the changes to the 0.4 version, in order:
1. Fixed a silly spelling error. It's "sorcerer", not "sorceror".
2. Added special class abilities into the feat window.
3. Added special items, tools and mounts to the equipment section.
4. Added racial skills to the skills window.
5. Gave halflings a +1 to saving throws.
6. Passed 5000 lines of code.



Version 0.3
====================================
The point of this release version was mostly to patch up the
bugs found by my wonderful team of testers. Thanks guys, you
make my live easier by poking at my flaws. Great, huh?

The changes made to the generator were, in order:
1. Fixed a bug that prevented sorcerers and wizards from
   having any starting money.
2. The window informing you of the skill cap limit, now
   appears in the skill window, rather than the main window.
3. Fixed bug with the Hide Skill. It seems that "HIDE" is a
   Java Script keyword...The skill has be redefined as "HIDE_SKILL".
   Rangers now have Hide as a class skill.
4. Fixed hit point problem. A first level character now gets
   full hit dice (+ con_mod).
5. Fixed problem with bad ability modifiers for non-human races.
6. You can now be any alignment. A little bug was keeping characters
   from being anything but lawful good.
7. Made pop-up window to allow the user to swap ability
   scores. This makes customizing a character easier and faster.
8. Now that the user can swap ability scores, no race modifiers
   are applied when you roll your ability scores. The race
   modifier is applied when you display your character.
9. Changed the define for hide armor from "HIDE" to "HIDE_ARMOR".
   This avoids the HIDE keyword in Java Script.
10. Added initiative, melee and ranged attack bonuses to
    the character sheet.



Version 0.2
====================================
This version has been polished a little. Actually, a lot.
A good deal of the generator was upgraded to version 3.5 from
3.0. The feats, skills and their requirements were changed to
the new 3.5 system. I hope I didn't break anything when I made
these changes. The next version (0.3) should have the new equipment
too. (Tada!) 0.2 now works under the Mozilla Web Browser. However, 
some of the new features don't work properly under Konqueror (sorry).
I'm hoping that other browsers, like IE and Opera, will also have
an easier time with the CG. Last, but not least, I fixed a bunch
of stupid mistakes. Like, you must now have one level in your
starting class and you can't un-select required feats without
un-selecting the more advanced feats. Oh, and the help windows
now actually display little bits of information. Please feel free
to send me suggestions on better help blurbs. I hope that this
version is easier to swallow. Thank you again for using.

The changes made to the CG were (in order):
1. Upgraded hit point calculation from 3.0 to 3.5.
2. Changed ability scores based on race.
3. Spell checked this file. Hopefully it's easier to read now.
4. Added menu bar to display/output windows. This should make it
   easier to print/save your character.
5. Wrote help windows. Clicking on a help button will give a 
   short description of what that part of the generator does.
6. Upgraded the number of skill points each class gets per level.
7. Upgraded skill names and requirements.
8. Upgraded feat names and requirements.
9. Made sure that the user may not remove feats that are
   required by other feats.
   (Note: Feat de-selection seems to be broken in Konqueror.)
10. Fixed problem with Mozilla. The CG now works with Mozilla 1.2.1.
    However, now, the CG does not work with Konqueror.
11. Moved the web address for the CG on-line to:
    http://slicer69.tripod.com/dnd/dnd.htm
    A copy of this file can be found at:
    http://slicer69.tripod.com/dnd/readme.txt
12. Made sure character has at least one level. Also, the user must
    take at least one level in his/her starting class.
13. Made skills colour coded. When you select your skills, you
    can now tell which ones are for your native class (green)
    and which are cross-class (blue.
14. Put a skill rank cap into effect. You may now select up to
    (3 + total level) ranks in native skills and half that in
    cross-class skills. Also made sure that each character starts
    with at least four skill points.

An additional thanks to BB for his suggestions/debugging.
Also, thank you to CD for pointing out some bugs and for
his suggestions.




Version 0.1
=================================
As the above states, this is version 0.1 of the generator.
It's more of the end of the beginning and I foresee a lot
more coding, revision and such to come. I would like to
thank you all for looking over this character generator,
hence known as a "CG". At this point, the CG covers most of
the basic ground. As it stands, the CG will run in most
popular web browsers with the exception of Netscape/Mozilla
or old versions of other browsers. These good-to-goes include
IE 5.5, IE 6.0, Opera 7.x, current versions of Safari and Konqueror 3.1. 
If you have a browser that does/doesn't work with the CG, please let me know.

The generator allows the user (that's you) to input some basic
information. This includes the character's race, class, level,
religion and name. You can also roll abilities, select skills, feats
and pick out your equipment.

The generator lets you display/print a blank character sheet or
a sheet with all of the field's filled in. The print/display may
look different on different browsers, so I didn't do anything
fancy here. I would like to suggest using a standard blank sheet and
use the CG to fill in the values....

As I mentioned before, this is a CG for 3rd Edition, not 3.5. The classes,
skills and misc items will have to be updated. If you find any "mistakes" where
stats should be updated, please be patient. I'll get around to it in future
releases. That being said, if you find any logic problems that are relevant
to 3.0, please let me know. A simple note like "A rogue's hit dice should
be a d6, not a d8" or "Kissing arse isn't a feat" will do.

I'd also like to add the disclaimer that not all of the equipment has
been entered into the generator. There are things like Gnome Science Lab
in the PHB that I decided to skip for now. If there are any requests, I'll
put that stuff in. I also have skipped all spells for this first pre-release.
That's a project in itself and I'm debating if it's an important part of
a CG.

Special thanks to IZ, KF and Topher for a lot of advice, (virtual) hand
holding and their technical support.

This CG is released under the GNU GPL (gpl.gnu.org). This basically means
that you can use and re-distribute this generator as you wish. 
So enjoy. Please send me (well documented) changes if you
make improvements to the code.

