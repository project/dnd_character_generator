<?php

/**
 * Search for the Drupal bootstrap so we can get the Database system working
 */
function drupalize() {
  while (!@stat('./includes/bootstrap.inc')) {
    chdir('..');
  }
  require_once './includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
}
drupalize();

if (!variable_get('dnd_character_generator_allow_save', 0)) {
  drupal_access_denied();
  drupal_page_footer();
  exit();
}

// verify that we got the correct ID and a request data file
if (!isset($_GET['id']) || !isset($_REQUEST['data'])) {
  header("HTTP/1.0 404 Not Found");
  exit();
}
$id = variable_get('dnd_character_generator_u' . $user->uid);
if ($_GET['id'] !== $id) {
  header("HTTP/1.0 404 Not Found");
  exit();
}

header('Content-Location: emails.csv');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="my-character.txt"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . strlen($_REQUEST['data']));
header('Pragma: private');
header('Cache-Control: private, must-revalidate');
header('Title: my-character.txt');
header('From: slicer69@hotmail.com');
header('Expires: Thu, 1 Jan 1970 00:00:00 GMT');

print $_REQUEST['data'];
