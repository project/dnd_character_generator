<?php

/**
 * @file
 * pages for display vocabularies, terms and nodes
 */


/**
 * Implementation of dnd_character_generator_page().
 */
function dnd_character_generator_page() {
  global $user;

  $path = drupal_get_path('module', 'dnd_character_generator');

  // With PHP 5.x+ we luckily get a new random number between each run!
  $id = md5(rand());

  // Save the form identifier in the database
  variable_set('dnd_character_generator_u' . $user->uid, $id);

  // The HTML file is fairly small (~30Kb)
  // However, the Javascript file is large (~400Kb)
  // The HTML takes care of loading the JS, but we need to tell it where it is
  return str_replace(
    array(
      '@DND_PATH@',
      '@ID@',
    ),
    array(
      base_path() . $path,
      $id,
    ),
    file_get_contents($path . '/dnd.form.html')
  );
}
