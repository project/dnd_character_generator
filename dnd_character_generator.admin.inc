<?php

/**
 * @file
 * administration form
 */


/**
 * Define the settings form
 */
function dnd_character_generator_settings() {
  // global settings
  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global'),
    '#description' => t('Global setup for the D&amp;D Character Generator'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['global']['dnd_character_generator_allow_save'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow saving via server'),
    '#default_value' => variable_get('dnd_character_generator_allow_save', 0),
  );

  return system_settings_form($form);
}
