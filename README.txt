

 About D&D Character Generator
===============================

This module provides access to the D&D Character Generator written by

     Jesse Smith <slicer69@hotmail.com>

The Drupal module was written by

     Alexis Wilke <alexis@m2osw.com>

The D&D Character Generator is a tool used to gnerate Dungeons & Dragons
characters written in Javascript.

Once loaded, the page does not use the server anymore. The entire Javascript
will be loaded on the clients computer and run there. Only the Save feature
eventually uses the server.

For detailed information about the generator, check out the dnd-readme.txt
file. It is mainly an overview of the enhancements in the generator.


 Installation
==============

  1. Install the files under sites/all/modules

  2. Turn on the D&D Character Generator module on the administer modules page

  3. Edit the settings in admin/settings/dnd_character_generator

  4. Configure access rights, by default users cannot access the new page

  5. Add a menu entry to access the /dnd_character_generator page

Note: The D&D Character Generator will fit in a page of 500px or more.
      (although the tables are marked to be 480px wide, it may get a bit
      larger in a couple of cases.)
      It will automatically be centered.
      It is NOT 100% xhtml compliant.


 Configuration
===============

  At this time, there is only one configuration: whether or not the
  users can Save via your server. By default, this feature is turn
  off for security reasons.

  The form will include a security feature in the form of the md5sum
  of a randomly generated number. That number is used to confirm that
  the user has the right to save via the server. This is a strong
  security method by it prevents the user from opening two windows
  and saving either one (he can only save the last one opened.)


 Using
=======

  Once installed, one can go to /dnd_character_generator to enter
  the generator. There is plainty of help on the page to teach you
  how to use it. Also the code tries to prevent you from making
  mistakes while generating your character.

